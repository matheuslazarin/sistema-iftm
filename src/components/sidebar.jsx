import React, { Component } from "react";

class SideBar extends Component {
  render() {
    return (
      <div className="wrapper">
        <div id="sidebar-header" onClick={this.hide}>
          <p>Painel de Navegação ⯅</p>
        </div>
        <nav id="sidebar">
          <ul className="list-unstyled">
            <li>
              <a href="#">Home</a>
            </li>
            <li id="li-people">
              <a href="#">Pessoas ▼</a>

              <ul className="list-unstyled">
                <li id="li-professor" className="display-none">
                  <a href="#">Docente</a>
                </li>
                <li id="li-techinician" className="display-none">
                  <a href="#">Técnico Administrativo</a>
                </li>
              </ul>
            </li>
            <li id="li-room-header">
              <a href="#">Salas ▼</a>
              <ul id="ul-room-header" className="list-unstyled">
                <li id="li-agenda" className="display-none">
                  <a href="#">Agenda</a>
                </li>
                <li id="li-time" className="display-none">
                  <a href="#">Horário</a>
                </li>
              </ul>
            </li>
            <li id="li-administration">
              <a href="#">Administração ▼</a>
              <ul className="list-unstyled">
                <li id="li-institution" className="display-none">
                  <a href="#">Instituição</a>
                </li>
                <li id="li-unity" className="display-none">
                  <a href="#">Unidade</a>
                </li>
                <li id="li-rooms" className="display-none">
                  <a href="#">Salas</a>
                </li>
                <li id="li-professors" className="display-none">
                  <a href="#">Docentes</a>
                </li>
                <li id="li-techinicians" className="display-none">
                  <a href="#">Tecnicos Administrativos</a>
                </li>
              </ul>
            </li>
          </ul>
        </nav>
      </div>
    );
  }
  hide() {
    let sidebar = document.getElementById("sidebar");
    if (sidebar.style.display === "none") {
      sidebar.style.display = "block";
    } else {
      sidebar.style.display = "none";
    }
  }
}

export default SideBar;
