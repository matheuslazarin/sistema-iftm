import React, { Component } from "react";

class NavBar extends Component {
  render() {
    return (
      <nav className="navbar-container">
        <div id="iftm-title">
          <span id="iftm-name">IFTM</span>
        </div>
        <div id="user-box-container" onClick={this.toggleUserInput}>
          <div id="user-box">
            <img src="../55089.png" alt="user" />
          </div>
        </div>
        <div id="user-input-box">
          <form>
            <label htmlFor="username">Usuário</label>
            <input
              type="text"
              className="form-control"
              id="username"
              name="username"
            />
            <label htmlFor="password" className="ml-2">
              Senha
            </label>
            <input
              className="form-control"
              type="text"
              name="password"
              id="password"
            />
            <button type="button" className="btn">
              Entrar
            </button>
          </form>
        </div>
      </nav>
    );
  }

  toggleUserInput() {
    let userInputBox = document.getElementById("user-input-box");
    if (userInputBox.style.display === "none") {
      userInputBox.style.display = "block";
    } else {
      userInputBox.style.display = "none";
    }
  }
}

export default NavBar;
