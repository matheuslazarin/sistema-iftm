import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <div className="footer-container">
        <div className="footer-text">
          Desenvolvido pelo Instituto Federal de Educação, Ciência e Tecnologia
          do Triângulo Mineiro – IFTM
        </div>
      </div>
    );
  }
}

export default Footer;
