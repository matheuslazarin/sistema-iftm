import React, { Component } from "react";
import NavBar from "./components/navbar";
import SideBar from "./components/sidebar";
import Footer from "./components/footer";
import "./App.css";
import Buttons from "./features-examples/buttons";
import Checkboxes from "./features-examples/checkboxes";
import RadioButtons from "./features-examples/radio-buttons";
import Selects from "./features-examples/selects";
import TextFields from "./features-examples/text-fields";
import Table from "./features-examples/table";
import Tab from "./features-examples/tab";
import Snackbars from "./features-examples/snackbars";

class App extends Component {
  render() {
    console.log("App - Rendered");
    return (
      <React.Fragment>
        {/* Comente e descomente para visualizar o layout do site */}
        <NavBar />
        <SideBar />
        <Footer />|
        {/* Comente e descomente para visualizar os exemplos dos componentes */}
        {/* <Buttons />
        <Checkboxes />
        <RadioButtons />
        <Selects />
        <TextFields />
        <Table />
        <Snackbars />
        <Tab /> */}
      </React.Fragment>
    );
  }
}

export default App;
